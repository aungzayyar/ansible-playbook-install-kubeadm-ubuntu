# Ansible playbook for installing Kubeadm on Ubuntu 16.04

Ref: https://kubernetes.io/docs/setup/independent/install-kubeadm/#installing-kubeadm-kubelet-and-kubectl

## Prerequisites
- ebtables
- ethtool
- docker

## Installation

Run the playbook

```bash
ansible-playbook site.yml
```
